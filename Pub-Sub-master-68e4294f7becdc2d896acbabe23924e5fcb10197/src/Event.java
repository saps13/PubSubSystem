import java.io.Serializable;

public class Event implements Serializable {
	public int id;
	public Topic topic;
	public String title;
	public String content;

	public Event(int i, Topic t, String ti, String con){
		id = i;
		topic = t;
		title = ti;
		content = con;
	}
}
