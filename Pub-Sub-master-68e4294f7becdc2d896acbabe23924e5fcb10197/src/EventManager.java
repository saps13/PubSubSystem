/**
 *EventManager.java
 *
 *Version: 1.0
 *
 *Revisons: 0
 */

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Event Manager is a server to which all the clients(Publisher and Subscribers) are connected. The clients are connected through the port number 5000.
 * This port number is only used for the initial connection only and then it responds to the client with a new port number and the client is connected
 * to the server with this new port number now. 
 */

public class EventManager implements Runnable {
    private static Map<Integer,ServerSocket> socketLoad;
    private static SharedData data;
    private static Thread thrd;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;
    private static Runnable[] thrdPool;
    private static ExecutorService pool;
    

    public EventManager(){
        pool = Executors.newFixedThreadPool(5);// Creating 5 threads in a pool under executor class
        thrdPool = new Runnable[5];// Thread intialization 
        data = new SharedData();// Creating an instance of SharedData class
        socketLoad = new HashMap<Integer,ServerSocket>();
        thrd = new Thread(this);
        for(int i = 0; i < 5; i++){
            try {
                socketLoad.put(i,new ServerSocket(8000+i));// Creating a serversocket with port nos starting from 8000-8004 
                thrdPool[i] = new EventManagerThread(socketLoad.get(i),this);// Assigning each server socket to each thread in the thread pool
            }
            catch(IOException e){ // To catch the IO Exception in case generated
                e.printStackTrace();
            }
        }
        thrd.start();// Starting the main thread 
    }
    public void startService(int port){
        try {
            ServerSocket ss = new ServerSocket(port);// To set up server socket connection and listening on the port 5000
            while(true) {
                System.out.println("Waiting for request");
                Socket cs = ss.accept();
                synchronized (data) {
                    data.addIP(cs.getInetAddress());// We save the IP address of the client connected to the Event Manager
                }
                System.out.println(cs.getLocalAddress()+" requested");
                oos = new ObjectOutputStream(cs.getOutputStream());// Setting up output stream
                ois = new ObjectInputStream(cs.getInputStream());// Setting up input stream
                Random rand = new Random();// Using random class instance
                int i = rand.nextInt(5);
                ServerSocket newSS = socketLoad.get(i);// Getting a random port number for the client to connect
                oos.writeObject(newSS.getLocalPort());// Sending the new port as a response to the client
                System.out.println("Connecting with "+cs.getRemoteSocketAddress()+ " at " + newSS.getLocalPort());
                //new EventManagerThread(newSS, this);
                pool.execute(thrdPool[i]);
            }
        }
        catch (IOException e){
            System.out.println("Connection Error");
            e.printStackTrace();
        }
    }

    //This is the main method
    public static void main(String[] args) {
        EventManager evnt = new EventManager();
        evnt.startService(5000);
    }
    //This is the method returns the topics 
    public void addTopic(Topic topic){
        synchronized (data) {
            data.addTopic(topic);
        }
    }
    //This is the method assigns the socket port to the client
    public ServerSocket getSocket(){
        Random rand = new Random();
        int i = rand.nextInt(5);
        return socketLoad.get(i);
    }
    //This method assigns the thread from the thread pool
    public Runnable getThread(){
        Random rand = new Random();
        int i = rand.nextInt(5);
        return thrdPool[i];
    }
    //This is the method returns the subscribers added
    public String addSubscriber(String key, InetAddress ina){
        synchronized (data) {
            return data.addSub(key, ina);
        }
    }
    //This is the method returns the events added
    public String addEvent(Event e){
        synchronized (data){
            return data.addEvent(e);
        }
    }
    //This is the method returns the topics unsubscribed
    public String unsubscribe(int i, InetAddress ina){
        synchronized (data){
            return data.removeSub(i,ina);
        }
    }

    public void run() {
        while (true) {
            List<InetAddress> ip;// This stores the list of IP addresses of clients
            List<Topic> t;// This stores the list of topics
            List<Event> ev;// This stores the list of events 
            Map<Integer,List<InetAddress>> mp; // This map 
            synchronized (data) {// In order to avoid corruption of data we synchronize the data
                ip = data.getIPList();
                t = data.getTopicList();
                ev = data.getEventList();
                mp = data.getMap();
            }
            int i = 0;
            while (i < ip.size()) {// To enter the loop there should be atleast one connection of client
                try {
                    Socket skt = new Socket(ip.get(i++).getHostAddress(), 7000);// Creating a socket port at 7000 to send notification to the client
                    ObjectOutputStream out = new ObjectOutputStream(skt.getOutputStream());// To send data 
                    ObjectInputStream in = new ObjectInputStream(skt.getInputStream());// To receive data
                    int j = (int)in.readObject();// 
                    out.writeObject(t.size() - j);
                    while(j < t.size()){
                        out.writeObject(t.get(j++));
                    }
                    int k = (int)in.readObject();
                    int c = 0,d= k;
                    while(k < ev.size()){
                        if(mp.containsKey(ev.get(k).topic.id) && mp.get(ev.get(k++).topic.id).contains(ip.get(i-1))){
                            c++;
                        }
                    }
                    out.writeObject(c);
                    while(d < ev.size()){

                        if(mp.get(ev.get(d).topic.id).contains(ip.get(i-1))){
                            out.writeObject(ev.get(d));
                        }
                        d++;
                    }
                    out.close();
                    skt.close();
                } catch (IOException e) {
                    System.out.println("Server unreachable at " + ip.get(i-1).getHostAddress());

                }catch (ClassNotFoundException e){
                    System.out.println("Incompatible data");
                }
                catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
            try {
                Thread.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
