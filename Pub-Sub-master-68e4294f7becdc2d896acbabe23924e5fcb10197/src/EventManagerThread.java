/**
 *EventManagerThread.java
 *
 *Version: 1.0
 *
 *Revisons: 0
 */



import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Event Manager Thread is a class whose functioning is to distribute the threads reserved for
 * the pub/sub clients in such a way so as to acheive the goal of multi socket thread program. 
 */


public class EventManagerThread implements Runnable {
    public Socket clientSocket;
    public ServerSocket serverSocket;
    public EventManager evnt;
    public ObjectOutputStream oos;
    public ObjectInputStream ois;
    public Thread thrd;
    

    public EventManagerThread(ServerSocket ss, EventManager ev){
        evnt = ev;// Intialises the event manager object instance
        serverSocket = ss;// Intialises the server socket object instance
    }
    public void run(){
        while (true) {
            connect();// starts the connection 
            try {
                switch ((String) ois.readObject()) {
                    case "Topic":
                        evnt.addTopic((Topic) ois.readObject());//calling the addTopic method in event manager
                        break;
                    case "Subscribe":
                        String response = evnt.addSubscriber((String) ois.readObject(), clientSocket.getInetAddress());//calling the addSubscriber method in event manager
                        oos.writeObject(response);
                        break;
                    case "Publish":
                        String res = evnt.addEvent((Event) ois.readObject());//calling the addEvent method in event manager
                        oos.writeObject(res);
                        break;
                    case "Unsubscribe":
                        String unsub = evnt.unsubscribe((int)ois.readObject(), clientSocket.getInetAddress());//calling the unsubscribe method in event manager
                        oos.writeObject(unsub);
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    public void connect(){
        try {
            clientSocket = serverSocket.accept();// to start the socket connection
            oos = new ObjectOutputStream(clientSocket.getOutputStream());
            ois = new ObjectInputStream(clientSocket.getInputStream());
            oos.writeObject("Connected");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
