/**
 *PubSubAgent.java
 *
 *Version: 1.0
 *
 *Revisons: 0
 */

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

/**
 * PubSubAgent is the class which represents the client side of our Pub/Sub System where both the
 * Publisher and the Subscirber use this class to do their functions of either publishing, 
 * advertising or subscribing. `
 */


public class PubSubAgent implements Runnable {

    public ObjectInputStream ois;
    public ObjectOutputStream oos;
    public Socket socket;
    public int port;
    public Thread thrd;
    public List<Topic> topics;
    public String host;


    public PubSubAgent(int p, String h){
        try {
            host = h;// Assign the hostname 
            thrd = new Thread(this);// Creating a new thread
            socket = new Socket(host, p);// Creating a socket with host name and port
            oos = new ObjectOutputStream(socket.getOutputStream());// Creating output stream
            ois = new ObjectInputStream(socket.getInputStream());// Creating input stream
            port = (int)ois.readObject();// Assigning the port number 
            ois.close();
            oos.close();
            socket.close();
            topics = Collections.synchronizedList(new ArrayList<Topic>());// Assignig topics for that Pub/Sub agent
            thrd.start();
        }
        catch(IOException e){
            //e.printStackTrace();
            System.out.println("Could not connect");
            System.exit(1);
        }
        catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        PubSubAgent agent = new PubSubAgent(5000, args[0]);// creating a PubSubAgent class instance 

        while (true) {
            System.out.println("Enter 1 to Advertise \nEnter 2 to Subscribe \nEnter 3 to Publish\nEnter 4 to Unsubscribe");
            Scanner scan = new Scanner(System.in);
            try {
                switch (scan.nextInt()) {
                    case 1:
                        System.out.println("Enter Topic");
                        String topic = scan.next();// Assign the topic name
                        List<String> key = new ArrayList<String>();// Creating the list of key elments for topics
                        key.add(topic);// adding the topics to the list key
                        while (!scan.hasNext("0")) {// Do a breakpoint when 0 is entered
                            key.add(scan.nextLine());
                        }
                        agent.advertise(new Topic(topic.hashCode(), key, topic));// Advertise method is called
                        break;
                    case 2:
                        System.out.println("Enter Topic");
                        String subTopic = scan.next();
                        agent.subscribe(subTopic);// Subscribe method is called
                        break;
                    case 3:
                        int i = 0;
                        List<Topic> temp;// a temp list to store the topics
                        synchronized (agent.topics) {
                            temp = agent.topics;
                        }
                        System.out.println("Enter Topic");
                        String t = scan.next();// user input for the topic
                        Topic tempTop = null;
                        while (i < temp.size()) {
                            if (temp.get(i).keywords.contains(t)) {// checking if topic is in the list of topics advertised
                                tempTop = temp.get(i);
                                System.out.println("Topic found");
                                break;
                            }

                            i++;
                        }
                        System.out.println("Enter Title");
                        String title = scan.next();
                        System.out.println("Enter Content");
                        String content = scan.next();
                        int id = (title + content).hashCode();// assign a hashcode based on the title and content 
                        Event event = new Event(id, tempTop, title, content);// Creating an entry in the event object
                        agent.publish(event);// calls the publish method
                        break;
                    case 4:
                        System.out.println("Enter Topic");
                        String s = scan.next();
                        List<Topic> tmp;// a temp list to store the topics
                        Topic tmpTop = null;
                        int j =0;
                        synchronized (agent.topics) {
                            tmp = agent.topics;
                        }
                        while (j < tmp.size()) {
                            if (tmp.get(j).keywords.contains(s)) {// checking if topic is in the list of topics advertised
                                tmpTop = tmp.get(j);
                                System.out.println("Topic found");
                                break;
                            }
                            j++;
                        }
                        agent.unsubscribe(tmpTop);// calls the unsubscribe method
                        break;
                }
            }catch(InputMismatchException e){
                System.out.println("Wrong Input");
            }
        }
    }
    //this method connects the pub/sub client to the event manager 
    public void connect(){
        try {
            socket = new Socket(host, port);// creates a socket connection 
            oos = new ObjectOutputStream(socket.getOutputStream());// output stream object creation
            ois = new ObjectInputStream(socket.getInputStream());// input stream object creation
            System.out.println((String)ois.readObject());
        }
        catch(IOException e){
            System.out.println("Connection error");
        }
        catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }
    // this method advertises the topics which client wants 
    public void advertise(Topic newTopic){
        connect();
        try {
            oos.writeObject("Topic");
            oos.writeObject(newTopic);
            closeConnection();
        } catch (IOException e) {
            System.out.println("Connection Error");
            System.exit(1);
        }
    }

    public void run(){
        try {
            ServerSocket ss = new ServerSocket(7000);//Assigning a new server socket at port 7000
            List<Event> events = new ArrayList<Event>();//Assigning a events list
            while(true) {
                Socket s = ss.accept();//Accepting new connection
                ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
                ObjectInputStream in = new ObjectInputStream(s.getInputStream());
                try {
                    synchronized (topics) {
                        out.writeObject(topics.size());//sending the topics size 
                        int i = (int) in.readObject();// receiving the topic object as int format
                        while (i > 0) {
                            Topic temp = (Topic) in.readObject();//converting it into topic object
                            topics.add(temp);// adding the topic name in topics list
                            System.out.println("New Topic: " + temp.name);
                            i--;
                        }
                    }
                    out.writeObject(events.size());// sends out the event list size
                    int i = (int) in.readObject();// receiving the event object as int format
                    while (i > 0) {
                        Event temp = (Event) in.readObject();//converting it into event object
                        events.add(temp);// adding it to the event list
                        System.out.println("Published: " + temp.topic.name + " " + temp.title);
                        i--;
                    }
                } catch (ClassNotFoundException e) {
                    System.out.println("Error: Incompatible type object");
                }
            }
        }
        catch(IOException e){
            System.out.println("Connection Error");
            //e.printStackTrace();
        }
    }
    //this method subscribes the topic which the client needs
    public void subscribe(String keyword){
        connect();
        try{
            oos.writeObject("Subscribe");
            oos.writeObject(keyword);
            System.out.println((String)ois.readObject());
            closeConnection();
        }catch (IOException e) {
            System.out.println("Connection Error");
            System.exit(1);
        }
        catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }
    //this method is to close the connection between the client and the event manager
    public void closeConnection(){
        try {
            oos.close();
            ois.close();
            socket.close();
        }
        catch(IOException e){
            System.out.println("Connection Error");
            System.exit(1);
        }
    }
    //this method is to publish any new content in the topic entered by the user
    public void publish(Event e){
        if(e.topic == null){
            System.out.println("Topic not found");
            return;
        }
        connect();
        try{
            oos.writeObject("Publish");
            oos.writeObject(e);
            System.out.println((String)ois.readObject());
            closeConnection();
        }catch (IOException io){
            System.out.println("Disconnected");
        }
        catch (ClassNotFoundException cl){
            System.out.println("Incompatible type object");
        }
    }
    //this method is to delete any subscriptions as needed by the client
    public void unsubscribe(Topic t){
        if(t == null){
            System.out.println("Topic not found");
            return ;
        }
        connect();
        try {
            oos.writeObject("Unsubscribe");
            oos.writeObject(t.id);
            System.out.println((String)ois.readObject());
        }
        catch(IOException e){
            System.out.println("Disconnected");
        }
        catch(ClassNotFoundException e){
            System.out.println("Incompatible type object");
        }
    }

}
