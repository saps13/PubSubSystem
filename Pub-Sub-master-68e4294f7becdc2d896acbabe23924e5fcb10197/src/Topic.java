import java.io.Serializable;
import java.util.List;

public class Topic implements Serializable{
	public int id;
	public List<String> keywords;
	public String name;
	public Topic(int i, List<String> l, String n){
		id = i;
		keywords = l;
		name = n;
	}
}